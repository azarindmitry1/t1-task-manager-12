package ru.t1.azarin.tm.api.controller;

public interface ICommandController {

    void showErrorCmd();

    void showErrorArg();

    void showVersion();

    void showAbout();

    void showHelp();

    void showInfo();

    void showArguments();

    void showCommands();

}