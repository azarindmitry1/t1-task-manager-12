package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}