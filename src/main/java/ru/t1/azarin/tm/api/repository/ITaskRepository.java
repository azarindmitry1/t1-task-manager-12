package ru.t1.azarin.tm.api.repository;

import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}