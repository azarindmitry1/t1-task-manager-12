package ru.t1.azarin.tm.api.service;

import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name);

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}