package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.repository.ICommandRepository;
import ru.t1.azarin.tm.constant.ArgumentConst;
import ru.t1.azarin.tm.constant.TerminalConst;
import ru.t1.azarin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display application version."
    );

    private static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    private static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display application command."
    );

    private static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display process and memory info."
    );

    private static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display arguments of application."
    );

    private static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display commands of application."
    );

    private static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Clear all projects."
    );

    private static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Show all projects."
    );

    private static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Show project by id."
    );

    private static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Show project by index"
    );

    private static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id."
    );

    private static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index."
    );

    private static Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start project by id."
    );

    private static Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );

    private static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );

    private static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    private static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    private static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Clear all tasks."
    );

    private static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show all tasks."
    );

    private static Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Show task by id."
    );

    private static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Show task by index"
    );

    private static Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update task by index"
    );

    private static Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id."
    );

    private static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index."
    );

    private static Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start task by id."
    );

    private static Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start task by index."
    );

    private static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );

    private static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );

    private static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            VERSION, ABOUT, HELP, INFO, ARGUMENTS, COMMANDS,

            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, PROJECT_SHOW_BY_ID,
            PROJECT_SHOW_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID,
            PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX,

            TASK_CREATE, TASK_CLEAR, TASK_LIST, TASK_SHOW_BY_ID,
            TASK_SHOW_BY_INDEX, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_START_BY_ID, TASK_START_BY_INDEX,
            TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}