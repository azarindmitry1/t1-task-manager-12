package ru.t1.azarin.tm.repository;

import ru.t1.azarin.tm.api.repository.ITaskRepository;
import ru.t1.azarin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findOneById(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}